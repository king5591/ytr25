import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-left',
  templateUrl: './left.component.html',
  styleUrls: ['./left.component.css'],
})
export class LeftComponent implements OnInit {
  @Input() price;
  @Input() rating;
  @Input() payment;
  @Output() selOption = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  emitChange() {
    this.selOption.emit();
  }
}
