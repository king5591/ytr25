/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Auth.gaurdService } from './auth.gaurd.service';

describe('Service: Auth.gaurd', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Auth.gaurdService]
    });
  });

  it('should ...', inject([Auth.gaurdService], (service: Auth.gaurdService) => {
    expect(service).toBeTruthy();
  }));
});
