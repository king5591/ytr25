import { NetService } from './../net.service';
import { Router } from '@angular/router';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.scss'],
})
export class FlightsComponent implements OnInit {
  @Input('flight') f1;
  @Input() selId;
  @Input() name;
  @Output() optChange = new EventEmitter();
  angel: boolean;
  data;
  str = '';
  url = 'https://us-central1-yvbty-ab7f2.cloudfunctions.net/app/booking';
  traveller;

  constructor(private router: Router, private netService: NetService) {}

  ngOnInit() {
    this.traveller = this.netService.data;
  }
  book(b1) {
    let obj = b1;
    obj['travellers'] = [];
    obj.travellers.push({
      name: 'Adult',
      count: this.traveller.traveller.adult,
    });
    obj.travellers.push({
      name: 'Child',
      count: this.traveller.traveller.child,
    });
    obj.travellers.push({
      name: 'Infant',
      count: this.traveller.traveller.infant,
    });
    obj['dept'] = this.traveller.dept;
    obj['arr'] = this.traveller.arr;
    obj['count'] = this.traveller.count;
    obj['pickDate'] = this.traveller.pickDate;
    obj['type'] = this.traveller.type;
    if (this.traveller.returnDate) {
      obj['returnDate'] = this.traveller.returnDate;
    }
    this.netService.postData(this.url, obj).subscribe(
      (resp) => {
        this.data = resp;
        // console.log(this.data);
      },
      (error) => {
        console.log(error);
      }
    );
    this.router.navigate(['/booking']);
  }

  flightDetails() {
    this.angel = !this.angel;
  }
  emitChange() {
    let obj = { action: this.name, item: this.f1 };
    this.optChange.emit(obj);
  }
}
