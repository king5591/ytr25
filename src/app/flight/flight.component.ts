import { NetService } from './../net.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css'],
})
export class FlightComponent implements OnInit {
  btn = 'one';
  data: string = 'Flights';
  city = ['New Delhi (DEL)', 'Mumbai (BOM)', 'Banglore (BLR)', 'Kolkata (CCU)'];
  depart;
  going;
  date;
  returnDate;
  isExpanded: boolean;
  traveller = 1;
  adult: number = 1;
  child: number = 0;
  infant: number = 0;
  bookClass = 'Economy';
  days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];
  mlist = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  calender: boolean;
  returnCalendar: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private netService: NetService
  ) {}

  ngOnInit() {
    this.date = new Date();
    this.returnDate = new Date(
      this.date.getFullYear(),
      this.date.getMonth(),
      this.date.getDate() + 1
    );
    // console.log(this.returnDate);
  }
  newDate(obj) {
    this.calender = false;
    this.date = new Date(obj.year, obj.month, obj.date);
    console.log(this.date);
  }
  newReturnDate(obj) {
    this.returnCalendar = false;
    this.returnDate = new Date(obj.year, obj.month, obj.date);
    console.log(this.returnDate);
  }
  showCalendar() {
    this.calender = true;
  }
  showReturnCalendar() {
    this.returnCalendar = true;
  }
  decrease(str) {
    if (str == 'adult') {
      this.adult--;
      this.traveller--;
    }
    if (str == 'child') {
      this.child--;
      this.traveller--;
    }
    if (str == 'infant') {
      this.infant--;
      this.traveller--;
    }
  }
  increase(str) {
    if (str == 'adult') {
      this.adult++;
      this.traveller++;
    }
    if (str == 'child') {
      this.child++;
      this.traveller++;
    }
    if (str == 'infant') {
      this.infant++;
      this.traveller++;
    }
  }
  toggle() {
    this.isExpanded = !this.isExpanded;
  }
  selectedWay(s) {
    this.btn = s;
  }
  type() {
    this.traveller = 1;
    this.adult = 1;
    this.child = 0;
    this.infant = 0;
  }
  matchCity() {
    if (this.depart == this.going) {
      alert('Destination and Arrival have to be different');
    }
  }
  searchFlight() {
    let d =
      this.date.getDate() +
      ' ' +
      this.mlist[this.date.getMonth()] +
      ' ' +
      this.date.getFullYear();
    let rd =
      this.returnDate.getDate() +
      ' ' +
      this.mlist[this.returnDate.getMonth()] +
      ' ' +
      this.returnDate.getFullYear();
    let obj;
    if (this.btn == 'return') {
      obj = {
        dept: this.depart,
        arr: this.going,
        pickDate: d,
        returnDate: rd,
        count: this.traveller,
        traveller: {
          adult: this.adult,
          child: this.child,
          infant: this.infant,
        },
        type: this.bookClass,
      };
    } else {
      obj = {
        dept: this.depart,
        arr: this.going,
        pickDate: d,
        count: this.traveller,
        traveller: {
          adult: this.adult,
          child: this.child,
          infant: this.infant,
        },
        type: this.bookClass,
      };
    }
    this.router.navigate(['/yatra/airSearch']);
    this.netService.data = obj;
  }
}
