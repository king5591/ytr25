import { Router, ActivatedRoute } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-all-hotel',
  templateUrl: './all-hotel.component.html',
  styleUrls: ['./all-hotel.component.css'],
})
export class AllHotelComponent implements OnInit {
  @Input('hotel') h1;
  n = 0;

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {}

  previous() {
    this.n--;
  }
  next() {
    this.n++;
  }
  chooseRoom(h1) {
    this.router.navigate(['/hotelDetails', h1.id], { relativeTo: this.route });
  }
}
