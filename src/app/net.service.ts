import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class NetService implements OnInit {
  cart = [];
  cartItem;
  data;
  numberOfItem;
  booked;
  constructor(private httpClient: HttpClient) {}
  ngOnInit(): void {
    this.cartItem = this.cart.length;
  }

  getData(url) {
    return this.httpClient.get(url);
  }
  postData(url, obj) {
    return this.httpClient.post(url, obj);
  }
  putData(url, obj) {
    return this.httpClient.put(url, obj);
  }
  deleteData(url) {
    return this.httpClient.delete(url);
  }
}
