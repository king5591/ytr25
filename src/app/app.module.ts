import { NetService } from './net.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { YatraComponent } from './yatra/yatra.component';
import { MyAccountComponent } from './myAccount/myAccount.component';
import { SupportComponent } from './support/support.component';
import { OffersComponent } from './offers/offers.component';
import { LeftPanelComponent } from './left-panel/left-panel.component';
import { ZippyComponent } from './zippy/zippy.component';
import { CalendarComponent } from './calendar/calendar.component';
import { HotelComponent } from './hotel/hotel.component';
import { FlightComponent } from './flight/flight.component';
import { AllFlightsComponent } from './all-flights/all-flights.component';
import { FiltersComponent } from './filters/filters.component';
import { FlightsComponent } from './flights/flights.component';
import { BookingComponent } from './booking/booking.component';
import { PaymentComponent } from './payment/payment.component';
import { SearchHotelComponent } from './search-hotel/search-hotel.component';
import { HotelDetailsComponent } from './hotel-details/hotel-details.component';
import { HotelBookingComponent } from './hotel-booking/hotel-booking.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { LeftComponent } from './left/left.component';
import { AllHotelComponent } from './all-hotel/all-hotel.component';

@NgModule({
  declarations: [											
    AppComponent,
    NavbarComponent,
    YatraComponent,
    MyAccountComponent,
    SupportComponent,
    OffersComponent,
    LeftPanelComponent,
    ZippyComponent,
    CalendarComponent,
    HotelComponent,
    FlightComponent,
      AllFlightsComponent,
      FiltersComponent,
      FlightsComponent,
      BookingComponent,
      PaymentComponent,
      SearchHotelComponent,
      HotelDetailsComponent,
      HotelBookingComponent,
      CheckoutComponent,
      LeftComponent,
      AllHotelComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [NetService],
  bootstrap: [AppComponent],
})
export class AppModule {}
