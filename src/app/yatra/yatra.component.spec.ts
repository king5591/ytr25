/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { YatraComponent } from './yatra.component';

describe('YatraComponent', () => {
  let component: YatraComponent;
  let fixture: ComponentFixture<YatraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YatraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YatraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
