import { NetService } from './../net.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-yatra',
  templateUrl: './yatra.component.html',
  styleUrls: ['./yatra.component.css'],
})
export class YatraComponent implements OnInit {
  flight;
  destination;
  data;
  url = 'http://localhost:2400/yatra';
  banner = 'https://i.ibb.co/Rc9qLyT/banner1.jpg';
  offers;
  constructor(private netService: NetService) {}

  ngOnInit() {
    this.netService.getData(this.url).subscribe((resp) => {
      this.data = resp;
      if (this.data) {
        this.flight = this.data.flight;
        this.destination = this.data.destination;
        this.offers = this.data.offers;
        // console.log(this.offers);
        // console.log(this.flight);
        // console.log(this.destination);
      }
    });
  }
}
