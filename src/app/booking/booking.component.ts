import { Router } from '@angular/router';
import { NetService } from './../net.service';
import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss'],
})
export class BookingComponent implements OnInit {
  formArr: FormGroup;
  arr: FormArray;
  data;
  travellers = [];
  flight;
  totalFare;
  url = 'https://us-central1-yvbty-ab7f2.cloudfunctions.net/app/booking';
  promo = {
    promo: [
      {
        applied: true,
        value: 1400,
        id: 'NEWPAY',
        disp:
          'Pay with PayPal to save upto Rs.1400 on Domestic flights (Max. discount Rs. 600 + 50% cashback up to Rs.800).',
      },
      {
        applied: false,
        value: 2000,
        id: 'YTAMZ19',
        disp:
          'Save upto Rs.2000 (Flat 6% (max Rs. 1,000) instant OFF + Flat 5% (max Rs. 1,000) cashback).',
      },
    ],
    select: '',
  };
  promoApplied;
  cancellation: boolean;
  cancelTick;
  protection = { name: 'Travel', check: false };
  protect;

  constructor(
    private netService: NetService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.formArr = fb.group({
      email: fb.control('', Validators.email),
      phone: fb.control('', [
        Validators.required,
        Validators.pattern('[0-9]*'),
      ]),
      arr: fb.array([this.details()]),
    });
  }

  ngOnInit() {
    this.data = this.netService.data;
    if (this.data) {
      console.log(this.data.traveller);
      for (let i = 1; i <= this.data.traveller.adult; i++) {
        this.travellers.push('Adult ' + i + ' :');
      }
      for (let i = 1; i <= this.data.traveller.child; i++) {
        this.travellers.push('Child ' + i + ' :');
      }
      for (let i = 1; i <= this.data.traveller.infant; i++) {
        this.travellers.push('Infant ' + i + ' :');
      }
      // console.log(this.travellers);
      for (let i = 0; i < this.travellers.length - 1; i++) {
        let temp = this.formArr.get('arr') as FormArray;
        temp.push(this.details());
      }
    }
    this.netService.getData(this.url).subscribe((resp) => {
      this.flight = resp;
      if (this.flight) {
        console.log(this.flight);
        this.calculateFare();
      }
    });
  }
  details(): FormGroup {
    return this.fb.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
    });
  }
  get email() {
    return this.formArr.get('email');
  }
  get phone() {
    return this.formArr.get('phone');
  }
  calculateFare() {
    if (!this.data.returnDate) {
      this.totalFare = this.flight.Price * this.data.count + 622;
      console.log(this.totalFare);
    } else {
      this.totalFare =
        this.flight.arrival.Price * this.data.count +
        this.flight.departure.Price * this.data.count +
        622;
      console.log(this.totalFare);
    }
  }
  promoAdd(p) {
    if (!this.promoApplied) {
      this.totalFare -= p.value;
    } else {
      let item = this.promo.promo.find((p1) => p1.id !== p.id);
      if (item) {
        this.totalFare += item.value;
      }
      this.totalFare -= p.value;
    }
    this.promoApplied = true;
  }
  protectAdd() {
    if (this.protect) {
      if (this.protection.check) {
        this.totalFare += 4408;
      } else {
        this.totalFare -= 4408;
      }
    } else {
      if (this.protection.check) {
        this.totalFare += 4408;
      }
    }
    this.protect = 'Protect';
  }
  cancelAdd() {
    if (this.cancelTick) {
      if (this.cancellation) {
        this.totalFare += 762;
      } else {
        this.totalFare -= 762;
      }
    } else {
      if (this.cancellation) {
        this.totalFare += 762;
      }
    }
    this.cancelTick = 'Cancel';
  }
  payment() {
    console.log('Inside');
    let passenger = [];
    let temp = this.formArr.controls.arr as FormArray;
    for (let i = 0; i < temp.length; i++) {
      passenger.push(temp.controls[i].value);
    }
    console.log(passenger);
    let count = passenger.length;
    let email1 = this.formArr.get('email').value;
    let mob = this.formArr.get('phone').value;
    let obj;
    if (!this.data.returnDate) {
      obj = {
        departure: this.flight,
        email: email1,
        mobile: mob,
        passengers: passenger,
        total: this.totalFare,
        travellers: count,
      };
    } else {
      obj = {
        arrival: this.flight.arrival,
        departure: this.flight.departure,
        email: email1,
        mobile: mob,
        passengers: passenger,
        total: this.totalFare,
        travellers: count,
      };
    }
    this.netService.booked = obj;
    this.router.navigate(['/payment']);
  }
}
