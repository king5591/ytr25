import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'zippy',
  templateUrl: './zippy.component.html',
  styleUrls: ['./zippy.component.css'],
})
export class ZippyComponent implements OnInit {
  @Input('title') title: string;
  @Input('traveller') traveller;
  @Input('room') room;
  isExpanded: boolean;
  constructor() {}

  ngOnInit() {}
  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
