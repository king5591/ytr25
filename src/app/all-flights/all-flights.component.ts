import { ActivatedRoute, Router } from '@angular/router';
import { NetService } from './../net.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-all-flights',
  templateUrl: './all-flights.component.html',
  styleUrls: ['./all-flights.component.scss'],
})
export class AllFlightsComponent implements OnInit {
  data;
  allFlights;
  returnFlight;
  url = 'https://us-central1-yvbty-ab7f2.cloudfunctions.net/app';
  priceStruct;
  timeStruct;
  airlineStruct;
  aircraftStruct;
  priceArr = ['0-5000', '5000-10000', '10000-15000', '15000-20000'];
  timeArr = ['0-6', '6-12', '12-18', '18-00'];
  airlineArr = ['Go Air', 'Indigo', 'SpiceJet', 'Air India'];
  aircraftArr = [
    'Airbus A320 Neo',
    'AirbusA320',
    'Boeing737',
    'Airbus A320-100',
  ];
  price: string;
  time: string;
  airline: string;
  aircraft: string;
  sort: string;
  arriveArrow: boolean;
  depArrow: boolean;
  priceArrow: boolean;
  selFlight;
  selRetFlight;
  selFId;
  selRId;
  totalFare = 0;

  constructor(
    private netService: NetService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.data = this.netService.data;
    console.log(this.data);
    this.route.queryParamMap.subscribe((param) => {
      this.price = param.get('price');
      this.time = param.get('time');
      this.airline = param.get('flight');
      this.aircraft = param.get('airbus');
      this.sort = param.get('sort');
      // console.log(this.price, this.time, this.airline, this.aircraft);
      this.makeStructure();
      if (
        this.price &&
        this.time &&
        this.airline &&
        this.aircraft &&
        this.sort
      ) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&time=' +
              this.time +
              '&flight=' +
              this.airline +
              '&airbus=' +
              this.aircraft +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.time && this.airline && this.aircraft) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&time=' +
              this.time +
              '&flight=' +
              this.airline +
              '&airbus=' +
              this.aircraft
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.time && this.airline && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&time=' +
              this.time +
              '&flight=' +
              this.airline +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.time && this.aircraft && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&time=' +
              this.time +
              '&airbus=' +
              this.aircraft +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.airline && this.aircraft && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&flight=' +
              this.airline +
              '&airbus=' +
              this.aircraft +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.time && this.airline && this.aircraft && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?time=' +
              this.time +
              '&flight=' +
              this.airline +
              '&airbus=' +
              this.aircraft +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.time && this.airline) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&time=' +
              this.time +
              '&flight=' +
              this.airline
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.time && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&time=' +
              this.time +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.time && this.aircraft) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&time=' +
              this.time +
              '&airbus=' +
              this.aircraft
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.airline && this.aircraft) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&flight=' +
              this.airline +
              '&airbus=' +
              this.aircraft
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.airline && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&flight=' +
              this.airline +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.aircraft && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&airbus=' +
              this.aircraft +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.time && this.airline && this.aircraft) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?time=' +
              this.time +
              '&flight=' +
              this.airline +
              '&airbus=' +
              this.aircraft
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.time && this.airline && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?time=' +
              this.time +
              '&flight=' +
              this.airline +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.time && this.aircraft && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?time=' +
              this.time +
              '&airbus=' +
              this.aircraft +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.airline && this.aircraft && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?flight=' +
              this.airline +
              '&airbus=' +
              this.aircraft +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.time) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&time=' +
              this.time
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.airline) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&flight=' +
              this.airline
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.aircraft) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&airbus=' +
              this.aircraft
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.time && this.airline) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?time=' +
              this.time +
              '&flight=' +
              this.airline
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.time && this.aircraft) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?time=' +
              this.time +
              '&airbus=' +
              this.aircraft
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.time && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?time=' +
              this.time +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.airline && this.aircraft) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?flight=' +
              this.airline +
              '&airbus=' +
              this.aircraft
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.airline && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?flight=' +
              this.airline +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.aircraft && this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?airbus=' +
              this.aircraft +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.price) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?price=' +
              this.price
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.time) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?time=' +
              this.time
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.airline) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?flight=' +
              this.airline
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.aircraft) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?airbus=' +
              this.aircraft
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else if (this.sort) {
        this.netService
          .getData(
            this.url +
              '/flights/' +
              this.data.dept +
              '/' +
              this.data.arr +
              '?sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      } else {
        this.netService
          .getData(
            this.url + '/flights/' + this.data.dept + '/' + this.data.arr
          )
          .subscribe((resp) => {
            this.allFlights = resp;
            console.log(this.allFlights);
          });
      }
      if (this.data) {
        if (this.data.returnDate) {
          if (
            this.price &&
            this.time &&
            this.airline &&
            this.aircraft &&
            this.sort
          ) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&time=' +
                  this.time +
                  '&flight=' +
                  this.airline +
                  '&airbus=' +
                  this.aircraft +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.time && this.airline && this.aircraft) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&time=' +
                  this.time +
                  '&flight=' +
                  this.airline +
                  '&airbus=' +
                  this.aircraft
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.time && this.airline && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&time=' +
                  this.time +
                  '&flight=' +
                  this.airline +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.time && this.aircraft && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&time=' +
                  this.time +
                  '&airbus=' +
                  this.aircraft +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.airline && this.aircraft && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&flight=' +
                  this.airline +
                  '&airbus=' +
                  this.aircraft +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.time && this.airline && this.aircraft && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?time=' +
                  this.time +
                  '&flight=' +
                  this.airline +
                  '&airbus=' +
                  this.aircraft +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.time && this.airline) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&time=' +
                  this.time +
                  '&flight=' +
                  this.airline
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.time && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&time=' +
                  this.time +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.time && this.aircraft) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&time=' +
                  this.time +
                  '&airbus=' +
                  this.aircraft
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.airline && this.aircraft) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&flight=' +
                  this.airline +
                  '&airbus=' +
                  this.aircraft
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.airline && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&flight=' +
                  this.airline +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.aircraft && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&airbus=' +
                  this.aircraft +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.time && this.airline && this.aircraft) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?time=' +
                  this.time +
                  '&flight=' +
                  this.airline +
                  '&airbus=' +
                  this.aircraft
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.time && this.airline && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?time=' +
                  this.time +
                  '&flight=' +
                  this.airline +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.time && this.aircraft && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?time=' +
                  this.time +
                  '&airbus=' +
                  this.aircraft +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.airline && this.aircraft && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?flight=' +
                  this.airline +
                  '&airbus=' +
                  this.aircraft +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.time) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&time=' +
                  this.time
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.airline) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&flight=' +
                  this.airline
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.aircraft) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&airbus=' +
                  this.aircraft
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.time && this.airline) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?time=' +
                  this.time +
                  '&flight=' +
                  this.airline
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.time && this.aircraft) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?time=' +
                  this.time +
                  '&airbus=' +
                  this.aircraft
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.time && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?time=' +
                  this.time +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.airline && this.aircraft) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?flight=' +
                  this.airline +
                  '&airbus=' +
                  this.aircraft
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.airline && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?flight=' +
                  this.airline +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.aircraft && this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?airbus=' +
                  this.aircraft +
                  '&sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.price) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?price=' +
                  this.price
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.time) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?time=' +
                  this.time
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.airline) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?flight=' +
                  this.airline
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.aircraft) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?airbus=' +
                  this.aircraft
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else if (this.sort) {
            this.netService
              .getData(
                this.url +
                  '/flights/' +
                  this.data.arr +
                  '/' +
                  this.data.dept +
                  '?sort=' +
                  this.sort
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          } else {
            this.netService
              .getData(
                this.url + '/flights/' + this.data.arr + '/' + this.data.dept
              )
              .subscribe((resp) => {
                this.returnFlight = resp;
                console.log(this.returnFlight);
                this.retStructure();
              });
          }
        }
      }
    });
  }
  retStructure() {
    this.selFId = this.allFlights[0].id;
    this.selRId = this.returnFlight[0].id;
    this.selFlight = this.allFlights[0];
    this.selRetFlight = this.returnFlight[0];
    this.totalFare = this.allFlights[0].Price + this.returnFlight[0].Price;
    console.log(this.selFlight);
    console.log(this.selRetFlight);
  }
  makeStructure() {
    this.airline = this.airline ? this.airline : '';
    this.aircraft = this.aircraft ? this.aircraft : '';
    this.priceStruct = {
      price: this.priceArr,
      selected: this.price ? this.price : '',
    };
    this.timeStruct = {
      time: this.timeArr,
      selected: this.time ? this.time : '',
    };
    this.airlineStruct = this.airlineArr.map((a1) => ({
      airline: a1,
      selected: false,
    }));
    this.aircraftStruct = this.aircraftArr.map((a1) => ({
      aircraft: a1,
      selected: false,
    }));
    let temp1 = this.airline.split(',');
    for (let i = 0; i < temp1.length; i++) {
      let item = this.airlineStruct.find((a1) => a1.airline === temp1[i]);
      if (item) {
        item.selected = true;
      }
    }
    let temp2 = this.aircraft.split(',');
    for (let i = 0; i < temp2.length; i++) {
      let item = this.aircraftStruct.find((a1) => a1.aircraft === temp2[i]);
      if (item) {
        item.selected = true;
      }
    }
  }
  otpChange() {
    let qparam = {};
    if (this.priceStruct.selected) {
      this.price = this.priceStruct.selected;
      qparam['price'] = this.price;
    }
    if (this.timeStruct.selected) {
      this.time = this.timeStruct.selected;
      qparam['time'] = this.time;
    }
    let temp1 = this.airlineStruct.filter((a1) => a1.selected);
    let flight = temp1.map((t1) => t1.airline);
    this.airline = flight.join(',');
    if (this.airline) {
      qparam['flight'] = this.airline;
    }
    let temp2 = this.aircraftStruct.filter((a1) => a1.selected);
    let airbus = temp2.map((t1) => t1.aircraft);
    this.aircraft = airbus.join(',');
    if (this.aircraft) {
      qparam['airbus'] = this.aircraft;
    }
    this.router.navigate(['/yatra/airSearch'], { queryParams: qparam });
  }
  sortBy(s) {
    let qparam = {};
    if (this.price) {
      qparam['price'] = this.price;
    }
    if (this.time) {
      qparam['time'] = this.time;
    }
    if (this.airline) {
      qparam['flight'] = this.airline;
    }
    if (this.aircraft) {
      qparam['airbus'] = this.aircraft;
    }
    if (s == 'arrival') {
      qparam['sort'] = s;
      this.arriveArrow = true;
      this.depArrow = false;
      this.priceArrow = false;
    } else if (s == 'depart') {
      qparam['sort'] = s;
      this.arriveArrow = false;
      this.depArrow = true;
      this.priceArrow = false;
    } else if (s == 'price') {
      qparam['sort'] = s;
      this.arriveArrow = false;
      this.depArrow = false;
      this.priceArrow = true;
    }
    this.router.navigate(['/yatra/airSearch'], { queryParams: qparam });
  }
  optRetChange(f1) {
    console.log(f1);
    if (f1.action == 'Dept') {
      this.selFlight = f1.item;
    }
    if (f1.action == 'Arr') {
      this.selRetFlight = f1.item;
    }

    this.totalFare = this.selFlight.Price + this.selRetFlight.Price;
  }
  book() {
    let item = { arrival: this.selFlight, departure: this.selRetFlight };
    let obj = item;
    obj['dept'] = this.data.dept;
    obj['arr'] = this.data.arr;
    obj['count'] = this.data.count;
    obj['pickDate'] = this.data.pickDate;
    obj['type'] = this.data.type;
    if (this.data.returnDate) {
      obj['returnDate'] = this.data.returnDate;
    }
    this.netService.postData(this.url + '/booking', obj).subscribe(
      (resp) => {
        this.data = resp;
        // console.log(this.data);
      },
      (error) => {
        console.log(error);
      }
    );
    this.router.navigate(['/booking']);
  }
}
