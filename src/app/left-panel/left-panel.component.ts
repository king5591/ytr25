import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.css'],
})
export class LeftPanelComponent implements OnInit {
  data: string = 'Hotels';
  sel = 2;
  constructor() {}

  ngOnInit() {}
  flight() {
    this.data = 'Flights';
    this.sel = 1;
  }
  hotels() {
    this.data = 'Hotels';
    this.sel = 2;
  }
  buses() {
    this.data = 'Buses';
    this.sel = 3;
  }
  taxis() {
    this.data = 'Taxis';
    this.sel = 4;
  }
}
