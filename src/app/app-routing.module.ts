import { HotelDetailsComponent } from './hotel-details/hotel-details.component';
import { SearchHotelComponent } from './search-hotel/search-hotel.component';
import { PaymentComponent } from './payment/payment.component';
import { BookingComponent } from './booking/booking.component';
import { AllFlightsComponent } from './all-flights/all-flights.component';
import { OffersComponent } from './offers/offers.component';
import { SupportComponent } from './_service/support/support.component';
import { MyAccountComponent } from './myAccount/myAccount.component';
import { YatraComponent } from './yatra/yatra.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: YatraComponent },
  { path: 'myAccount', component: MyAccountComponent },
  { path: 'support', component: SupportComponent },
  { path: 'offers', component: OffersComponent },
  { path: 'yatra', component: YatraComponent },
  { path: 'yatra/airSearch', component: AllFlightsComponent },
  { path: 'booking', component: BookingComponent },
  { path: 'payment', component: PaymentComponent },
  { path: 'hotelSearch/:city', component: SearchHotelComponent },
  { path: 'hotelDetails/:id', component: HotelDetailsComponent },
  { path: '', redirectTo: 'yatra', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
