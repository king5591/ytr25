import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  @Input('price') price;
  @Input('time') time;
  @Input('airline') airline;
  @Input('aircraft') aircraft;
  @Output() selOption = new EventEmitter();
  mainExpand: boolean;
  filter: boolean;

  constructor(private router: Router) {}

  ngOnInit() {}
  cancel() {
    this.mainExpand = false;
  }
  show() {
    this.mainExpand = true;
  }
  emitChange() {
    let temp1 = this.airline.filter((a1) => a1.selected);
    let temp2 = this.aircraft.filter((a1) => a1.selected);
    if (
      this.price.selected ||
      this.time.selected ||
      temp1.length ||
      temp2.length
    ) {
      this.filter = true;
    } else {
      this.filter = false;
    }
    // console.log(this.filter);
    this.mainExpand = false;
    this.selOption.emit();
  }
  clearFilter() {
    this.filter = false;
    this.mainExpand = false;
    this.price = { price: this.price.price, selected: '' };
    this.time = { time: this.time.time, selected: '' };
    this.airline = this.airline.map((a1) => ({
      airline: a1.airline,
      selected: false,
    }));
    this.aircraft = this.aircraft.map((a1) => ({
      aircraft: a1.aircraft,
      selected: false,
    }));
    this.router.navigate(['/yatra/airSearch'], {
      queryParams: { price: null, time: null, flight: null, airbus: null },
    });
  }
}
