import { Router, ActivatedRoute } from '@angular/router';
import { NetService } from './../net.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-hotel',
  templateUrl: './search-hotel.component.html',
  styleUrls: ['./search-hotel.component.css'],
})
export class SearchHotelComponent implements OnInit {
  data;
  url = 'http://localhost:2400/hotel';
  city;
  priceArr = [
    'Less than Rs. 2000',
    'Rs. 2001 to Rs. 4000',
    'Rs. 4001 to Rs. 7000',
    'Rs. 7001 to Rs. 10000',
    'Greater than Rs. 10001',
  ];
  rating = [1, 2, 3, 4, 5];
  payment = ['UPI', 'Credit Card', 'Debit Card', 'Net Banking', 'PayPals'];
  priceStruct;
  rateStruct;
  payStruct;
  price = '';
  pay = '';
  rate = '';
  sort = '';

  constructor(
    private netService: NetService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((param) => {
      this.city = param.get('city');
    });
    this.route.queryParamMap.subscribe((param) => {
      this.price = param.get('price');
      this.rate = param.get('rating');
      this.pay = param.get('payment');
      this.makeStructure();
      console.log(this.price, this.rate, this.pay);
      if (this.price && this.rate && this.pay && this.sort) {
        this.netService
          .getData(
            this.url +
              '/' +
              this.city +
              '?price=' +
              this.price +
              '&payment=' +
              this.pay +
              '&rating=' +
              this.rate +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.price && this.rate && this.pay) {
        this.netService
          .getData(
            this.url +
              '/' +
              this.city +
              '?price=' +
              this.price +
              '&payment=' +
              this.pay +
              '&rating=' +
              this.rate
          )
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.price && this.rate && this.sort) {
        this.netService
          .getData(
            this.url +
              '/' +
              this.city +
              '?price=' +
              this.price +
              '&rating=' +
              this.rate +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.price && this.pay && this.sort) {
        this.netService
          .getData(
            this.url +
              '/' +
              this.city +
              '?price=' +
              this.price +
              '&payment=' +
              this.pay +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.rate && this.pay && this.sort) {
        this.netService
          .getData(
            this.url +
              '/' +
              this.city +
              '?payment=' +
              this.pay +
              '&rating=' +
              this.rate +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.price && this.rate) {
        this.netService
          .getData(
            this.url +
              '/' +
              this.city +
              '?price=' +
              this.price +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.price && this.pay) {
        this.netService
          .getData(
            this.url +
              '/' +
              this.city +
              '?payment=' +
              this.pay +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.price && this.sort) {
        this.netService
          .getData(
            this.url +
              '/' +
              this.city +
              '?price=' +
              this.price +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.rate && this.pay) {
        this.netService
          .getData(
            this.url +
              '/' +
              this.city +
              '?payment=' +
              this.pay +
              '&rating=' +
              this.rate
          )
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.rate && this.sort) {
        this.netService
          .getData(
            this.url +
              '/' +
              this.city +
              '?rating=' +
              this.rate +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.pay && this.sort) {
        this.netService
          .getData(
            this.url +
              '/' +
              this.city +
              '?payment=' +
              this.pay +
              '&sort=' +
              this.sort
          )
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.price) {
        this.netService
          .getData(this.url + '/' + this.city + '?price=' + this.price)
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.rate) {
        this.netService
          .getData(this.url + '/' + this.city + '?rating=' + this.rate)
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.pay) {
        this.netService
          .getData(this.url + '/' + this.city + '?payment=' + this.pay)
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else if (this.sort) {
        this.netService
          .getData(this.url + '/' + this.city + '?sort=' + this.sort)
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      } else {
        this.netService
          .getData(this.url + '/' + this.city)
          .subscribe((resp) => {
            this.data = resp;
            console.log(this.data);
          });
      }
    });
  }
  makeStructure() {
    this.priceStruct = {
      price: this.priceArr,
      selected: this.price ? this.price : '',
    };
    this.payStruct = {
      payment: this.payment,
      selected: this.pay ? this.pay : '',
    };
    this.rateStruct = {
      rating: this.rating,
      selected: this.rate ? this.rate : '',
    };
  }

  optChange() {
    let qparam = {};
    if (this.priceStruct.selected) {
      this.price = this.priceStruct.selected;
      qparam['price'] = this.price;
    }
    if (this.rateStruct.selected) {
      this.rate = this.rateStruct.selected;
      qparam['rating'] = this.rate;
    }
    if (this.payStruct.selected) {
      this.pay = this.payStruct.selected;
      qparam['payment'] = this.pay;
    }
    this.router.navigate(['/hotelSearch', this.city], {
      relativeTo: this.route,
      queryParams: qparam,
    });
  }

  sortBy(s) {}
}
