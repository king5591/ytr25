import { NetService } from './../net.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css'],
})
export class PaymentComponent implements OnInit {
  data;
  payMethod = ['UPI', 'Credit Card', 'Debit Card', 'Net Banking', 'PayPal'];
  payOption: 'UPI';
  total = 0;
  constructor(private netService: NetService) {}

  ngOnInit() {
    this.data = this.netService.booked;
    console.log(this.data);
    this.payOption = 'UPI';
    if (this.data) {
      this.total = this.data.total;
      this.total += 350;
    }
  }
  pay(p) {
    this.payOption = p;
  }
}
