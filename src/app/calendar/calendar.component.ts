import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {
  @Input('calendar') calendar;
  @Input('month') month;
  @Input('year') year;
  @Input('date') dates;
  @Output('newDate') selDate = new EventEmitter();
  date;
  day;
  selectedDate;
  mlist = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  days = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
  totalDay;

  constructor() {}

  ngOnInit() {
    let a = 1;
    this.selectedDate = this.dates;
    this.day = new Date('1 Feb 2021').getDay();
    console.log(this.day);
    this.totalDay = new Date(this.year, this.month + 1, 0).getDate();
    let temp1 = [];
    for (let i = 0; i < 6; i++) {
      let temp2 = [];
      for (let j = 0; j < 7; j++) {
        if (i == 0 && this.day == j) {
          temp2[j] = a;
          a++;
        } else if (i == 0 && j > this.day) {
          temp2[j] = a;
          a++;
        } else if (i != 0 && a <= this.totalDay) {
          temp2[j] = a;
          a++;
        } else {
          temp2[j] = 0;
        }
      }
      temp1[i] = temp2;
    }
    this.date = temp1;
    console.log(this.date);
  }
  selectDate(d) {
    this.selectedDate = d;
    this.selDate.emit({ date: d, month: this.month, year: this.year });
  }
  next() {
    this.month++;

    if (this.month > 11) {
      this.month = 0;
      this.year++;
    }
    let a = 1;
    this.totalDay = new Date(this.year, this.month + 1, 0).getDate();
    let temp1 = [];
    for (let i = 0; i < 6; i++) {
      let temp2 = [];
      for (let j = 0; j < 7; j++) {
        if (i == 0 && this.day == j) {
          temp2[j] = a;
          a++;
        } else if (i == 0 && j > this.day) {
          temp2[j] = a;
          a++;
        } else if (i != 0 && a <= this.totalDay) {
          temp2[j] = a;
          a++;
        } else {
          temp2[j] = 0;
        }
      }
      temp1[i] = temp2;
    }
    this.date = temp1;
    console.log(this.date);
  }
  previous() {
    this.month--;
    if (this.month < 0) {
      this.month = 11;
      this.year--;
    }
    let a = 1;
    this.totalDay = new Date(this.year, this.month + 1, 0).getDate();
    let temp1 = [];
    for (let i = 0; i < 6; i++) {
      let temp2 = [];
      for (let j = 0; j < 7; j++) {
        if (i == 0 && this.day == j) {
          temp2[j] = a;
          a++;
        } else if (i == 0 && j > this.day) {
          temp2[j] = a;
          a++;
        } else if (i != 0 && a <= this.totalDay) {
          temp2[j] = a;
          a++;
        } else {
          temp2[j] = 0;
        }
      }
      temp1[i] = temp2;
    }
    this.date = temp1;
    console.log(this.date);
  }
}
