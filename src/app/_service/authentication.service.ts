import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  currentUserValue: boolean = false;
  username: string = null;
  private loginStatusObs = new Subject<boolean>();
  private loginStatusObs$ = this.loginStatusObs.asObservable();

  constructor() {}
  login(name: string, password: string) {
    this.currentUserValue = true;
    this.username = name;
    this.loginStatusObs.next(this.currentUserValue);
  }
  logout() {
    this.currentUserValue = false;
    this.username = null;
    this.loginStatusObs.next(this.currentUserValue);
  }
}
