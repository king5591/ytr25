import { NetService } from './../net.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css'],
})
export class HotelComponent implements OnInit {
  locations = [
    { display: 'New Delhi, Delhi, India (3603 hotels)', value: 'New Delhi' },
    {
      display: 'Bengaluru, Karnataka, India (2781 hotels)',
      value: 'Bengaluru',
    },
    { display: 'Mumbai, Maharashtra, India (3188 hotels)', value: 'Mumbai' },
    { display: 'Pune, Maharashtra, India (1419 hotels)', value: 'Pune' },
    { display: 'Jaipur, Rajasthan, India (1822 hotels)', value: 'Jaipur' },
    { display: 'Goa, Goa, India (4125 hotels)', value: 'Goa' },
    { display: 'Kolkata, West Bengal, India (2466 hotels)', value: 'Kolkata' },
    { display: 'Bangkok, Thailand', value: 'Bangkok' },
    { display: 'Singapore, Singapore', value: 'Singapore' },
  ];
  rooms = [{ adult: 2, child: 0 }];
  loc: string;
  date;
  returnDate;
  isExpanded: boolean;
  traveller;
  bookClass = 'Economy';
  days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];
  mlist = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  calender: boolean;
  returnCalendar: boolean;

  constructor(
    private router: Router,
    private netService: NetService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.traveller = this.rooms.reduce(
      (acc, rcc) => acc + rcc.adult + rcc.child,
      0
    );
    console.log(this.traveller);
    this.loc = this.locations[0].value;
    this.date = new Date();
    this.returnDate = new Date(
      this.date.getFullYear(),
      this.date.getMonth(),
      this.date.getDate() + 1
    );
    console.log(this.returnDate);
  }

  newDate(obj) {
    this.calender = false;
    this.date = new Date(obj.year, obj.month, obj.date);
    console.log(this.date);
  }
  newReturnDate(obj) {
    this.returnCalendar = false;
    this.returnDate = new Date(obj.year, obj.month, obj.date);
    console.log(this.returnDate);
  }
  showCalendar() {
    this.calender = true;
  }
  showReturnCalendar() {
    this.returnCalendar = true;
  }
  decrease(str, i) {
    if (str == 'adult') {
      this.rooms[i].adult--;
      this.traveller--;
    }
    if (str == 'child') {
      this.rooms[i].child--;
      this.traveller--;
    }
  }
  increase(str, i) {
    if (str == 'adult') {
      this.rooms[i].adult++;
      this.traveller++;
    }
    if (str == 'child') {
      this.rooms[i].child++;
      this.traveller++;
    }
  }
  addRoom() {
    this.rooms.push({ adult: 2, child: 0 });
    // console.log(this.rooms);
    this.traveller = this.rooms.reduce(
      (acc, rcc) => acc + rcc.adult + rcc.child,
      0
    );
  }
  removeRoom() {
    this.rooms.pop();
    // console.log(this.rooms);
    this.traveller = this.rooms.reduce(
      (acc, rcc) => acc + rcc.adult + rcc.child,
      0
    );
  }
  searchRoom() {
    console.log(this.loc);
    this.router.navigate(['/hotelSearch', this.loc], {
      relativeTo: this.route,
    });
  }
}
